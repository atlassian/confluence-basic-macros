package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * A simple macro to put anchors into the page
 */
public class AnchorMacro extends BaseMacro implements Macro {

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context) {
        return TokenType.INLINE;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) {
        return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) {
        String anchorName = defaultString(parameters.get("0"));

        return StringUtils.isNotBlank(anchorName)
                ? "<span class=\"confluence-anchor-link\" id=\"" +
                AbstractPageLink.generateAnchor(conversionContext.getPageContext(), anchorName) +
                "\">" +
                escapeHtml4(body) + /* To prevent XSS */
                "</span>"
                : "";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
