package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.v2.components.HtmlEscaper;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

public class NoformatMacro implements Macro {
    private final com.atlassian.renderer.v2.macro.Macro oldNoformatMacro;

    public NoformatMacro(@ComponentImport SubRenderer v2SubRenderer) {
        this.oldNoformatMacro = new com.atlassian.renderer.v2.macro.basic.NoformatMacro(v2SubRenderer);
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        // The V2 renderer would auto-escape bodies which we do not do in the XHTML renderer so we need to do it here
        // instead (since the oldCodeMacro) expects it.
        body = HtmlEscaper.escapeAll(body, false);

        try {
            PageContext pageContext = (context == null) ? null : context.getPageContext();
            return oldNoformatMacro.execute(parameters, body, pageContext);
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
