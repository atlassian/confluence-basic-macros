package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.Macro;

import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class NoLinkMacro extends BaseMacro {

    @Override
    public boolean isInline() {
        return true;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(final Map parameters, final String body, final RenderContext renderContext) {
        final String linkParam = (String) parameters.get(Macro.RAW_PARAMS_KEY);
        return isNotEmpty(linkParam) ? GeneralUtil.escapeXml(linkParam) : "";
    }
}
