package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * @since 3.1.0
 */
public final class PanelBasicMacro extends com.atlassian.renderer.v2.macro.basic.PanelMacro implements Macro {

    public String execute(
            final Map<String, String> parameters,
            final String body,
            final ConversionContext conversionContext) throws MacroExecutionException {
        try {
            return execute(parameters, body, conversionContext.getPageContext());
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
