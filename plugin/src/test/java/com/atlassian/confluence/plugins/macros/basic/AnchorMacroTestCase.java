package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.RenderMode;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AnchorMacroTestCase {

    private AnchorMacro anchorMacro;

    @Before
    public void setUp() {
        anchorMacro = new AnchorMacro();
    }

    @Test
    public void testMacroIsInline() {
        assertEquals(Macro.OutputType.INLINE, anchorMacro.getOutputType());
    }

    @Test
    public void testMacroHasNoBody() {
        assertFalse(anchorMacro.hasBody());
    }

    @Test
    public void testRenderBodyInline() {
        assertEquals(RenderMode.NO_RENDER, anchorMacro.getBodyRenderMode());
    }

    @Test
    public void testRenderMacroWhenThereIsNoMacroParameter() {
        final Map<String, String> macroParameters = new HashMap<>();
        final Page pageToBeRendered = new Page();

        assertEquals(StringUtils.EMPTY, anchorMacro.execute(macroParameters, StringUtils.EMPTY, pageToBeRendered.toPageContext()));
    }

    @Test
    public void testAnchorNameEncoded() {
        final Map<String, String> macroParameters = new HashMap<>();
        final Page pageToBeRendered = new Page();
        final String htmlUnsafeString = "Insert <HTML unsafe characters> here.";

        macroParameters.put("0", htmlUnsafeString);

        assertEquals("<span class=\"confluence-anchor-link\" id=\"" + AbstractPageLink.generateAnchor(pageToBeRendered.toPageContext(), htmlUnsafeString) + "\"></span>",
                anchorMacro.execute(macroParameters, StringUtils.EMPTY, pageToBeRendered.toPageContext()));
    }
}
