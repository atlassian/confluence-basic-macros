package it.com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.lang.String.format;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

@RunWith(ConfluenceStatelessTestRunner.class)
public class NoFormatMacroStatelessTest {

    private static final String MACRO_NAME = "noformat";
    private static final String PAGE_CONTENT = format("{%s}*bold*%n_italic_%n-strike-%n{%s}", MACRO_NAME, MACRO_NAME);

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .space(space)
            .author(user)
            .title("NoFormat Macro Stateless Test")
            .content(PAGE_CONTENT, ContentRepresentation.WIKI)
            .build();

    @Test
    public void testNoFormat() {
        product.loginAndView(user.get(), page.get());
        waitUntil(
                finder.find(By.xpath("//div[@class='wiki-content']//div[contains(@class, 'preformatted panel')]//pre")).timed().getText(),
                allOf(
                        containsString("*bold*"),
                        containsString("_italic_"),
                        containsString("-strike-")
                )
        );
    }

}
